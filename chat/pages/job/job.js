// pages/job/job.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    val: '',
    arr:[]
  },

  jump() {
    console.log(123)
    wx.navigateTo({
      url: '../about/about'
    })
  },
  getvalue(e){
   this.setData({
     val: e.detail.value
   })
  },
  onTap(){
    let arr1=this.data.arr
    arr1.push(this.data.val)
    this.setData({
      arr:arr1
    })
    console.log(this.data.arr)
  },
  ondelete(e) {
    let arr2=this.data.arr;
    arr2.splice(e.target.dataset.id, 1)
    this.setData({
      arr:arr2
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({ title: "职位" }) 
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})